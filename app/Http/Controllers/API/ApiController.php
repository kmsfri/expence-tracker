<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use League\Fractal;
use League\Fractal\Serializer\SerializerAbstract;
use League\Fractal\Serializer\DataArraySerializer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Libs\Transformers\Transformer;
use Illuminate\Database\Eloquent\Collection;
use JWTAuth;

class ApiController extends Controller
{
    protected $status_code = 200;

    protected $message = null;

    protected $meta = [];

    protected $fractal = null;

    /**
     * ApiController constructor.
     */
    public function __construct()
    {

        $this->fractal = new Fractal\Manager();

        $this->setSerializer(new DataArraySerializer);

        $request = app('request');
        $user = !empty(JWTAuth::getToken()) ? JWTAuth::parseToken()->toUser() : Null;
        $request->request->add(['user' => $user]);

    }

    /**
     * @param $code
     * @return $this
     */
    protected function setStatusCode($code)
    {
        $this->status_code = $code;

        return $this;
    }

    /**
     * @return int
     */
    protected function getStatusCode()
    {
        return $this->status_code;
    }

    /**
     * @param $message
     * @return $this
     */
    protected function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return null
     */
    protected function getMessage()
    {
        return $this->message;
    }

    /**
     * @param $meta
     * @return $this
     */
    protected function setMeta($meta)
    {

        if (!is_array($meta))
            $meta = [$meta];

        $this->meta = array_merge($this->meta, $meta);

        return $this;
    }

    /**
     * @return array
     */
    protected function getMeta()
    {
        return $this->meta;
    }

    /**
     * @param SerializerAbstract $serializer
     * @return $this
     */
    protected function setSerializer(SerializerAbstract $serializer)
    {
        $this->fractal->setSerializer($serializer);

        return $this;
    }

    /**
     * @return SerializerAbstract
     */
    protected function getSerializer()
    {
        return $this->fractal->getSerializer();
    }

    /**
     * @return ApiController
     */
    protected function respondNotFound()
    {
        return $this->respondWithError(trans('api/messages.not_found'));
    }

    /**
     * @param Validator $validator
     * @return ApiController
     */
    protected function respondValidationError(Validator $validator)
    {
        $errors['errors'] = $validator->errors()->getMessages();
        $this->setMeta($errors);

        $message = collect($errors)->flatten()->first();

        return $this->respondWithError($message, 422);
    }

    /**
     * @param $message
     * @param int $code
     * @param array $data
     * @return ApiController
     */
    protected function respondWithError($message, $code = 404, $data = [])
    {
        $this->setMessage($message);
        $this->setStatusCode($code);
        return $this->respond(['data' => $data]);
    }

    protected function respondWithMessage($message, $code = 200)
    {
        return $this->setMessage($message)
            ->setStatusCode($code)
            ->respond(['data' => []]);
    }

    /**
     * @param null $data
     * @param array $headers
     * @return $this
     */
    protected function respond($data = null, $headers = [], $cookies = [])
    {
        if (is_null($data))
            $data['data'] = [];

        if (!is_array($data))
            $data = [$data];

        $status['status'] = [
            'status_code' => $this->getStatusCode(),
            'message' => $this->getMessage()
        ];
        if (Arr::has($data, 'meta.pagination')) {

            $this->setMeta(array_except($data['meta'], 'pagination.links'));
        }
        $this->setMeta($status);
        $data['meta'] = $this->getMeta();

        $response = response()
            ->json($data, $this->getStatusCode())
            ->withHeaders($headers);
        if (!empty($cookies)) {
            $response->withCookie(setInfoCookies($cookies));
        }
        return $response;
    }

    /**
     * @param $data collection | Item
     * @param Transformer $transformer
     * @param null $includes
     * @return $this
     */
    protected function respondWithTransform($data, Transformer $transformer, $includes = null, $cookies = [])
    {
        $response = $this->transform($data, $transformer, $includes);

        return $this->respond($response, [], $cookies);
    }

    /**
     * @param $data collection | Item
     * @param Transformer $transformer
     * @param null $includes
     * @return array
     */
    protected function transform($data, Transformer $transformer)
    {
        if ($data instanceOf Collection) {
            $resource = new Fractal\Resource\Collection($data, $transformer);
        } else {
            $resource = new Fractal\Resource\Item($data, $transformer);
        }
        return $resource->getTransformer()->getFractal()->createData($resource)->toArray();
    }

    /**
     * @param LengthAwarePaginator $paginator
     * @param Transformer $transformer
     * @param null $queryString From Request
     * @return array
     */
    protected function transformWithPagination(LengthAwarePaginator $paginator, Transformer $transformer, $queryString = null)
    {

        if (!is_null($queryString)) {
            parse_str($queryString, $queryStringArray);
            if (array_has($queryStringArray, 'include')) {
                $this->fractal->parseIncludes($queryStringArray['include']);
            }
            $queryParams = array_diff_key($queryStringArray, array_flip(['page']));
            $paginator->appends($queryParams);
        }

        $data = $paginator->getCollection();

        if ($data instanceOf Collection) {

            $resource = new Fractal\Resource\Collection($data, $transformer);
            $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
            return $this->fractal->createData($resource)->toArray();
        }

        return [
            'data' => []
        ];

    }

    /**
     * @param LengthAwarePaginator $paginator
     * @param Transformer $transformer
     * @param null $queryString
     * @return $this
     */
    protected function respondWithPagination(LengthAwarePaginator $paginator, Transformer $transformer, $queryString = null)

    {
        $response = $this->transformWithPagination($paginator, $transformer, $queryString);
        return $this->respond($response);
    }
}
