<?php

namespace App\Http\Controllers\API\V1\Users;

use App\Http\Controllers\API\ApiController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;
use Exception;
use Validator;
use App\Models\User;


class AuthController extends ApiController
{

    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="User Authenticate API's Documentation(ٰVersion 1)",
     *      description="This class will be used for user registration and authentication. The JWT method is used here",
     *      @OA\License(
     *          name="Apache 2.0",
     *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *      )
     * )
     */

    /**
     * @OA\Post(
     *     path="/api/v1/login",
     *     summary="User login",
     *     tags={"login"},
     *     description="This method receives the email and password and then logins the user",
     *     operationId="loginUserByEmailAndPass",
     *     @OA\Parameter(
     *         name="email",
     *         in="query",
     *         description="User's Email",
     *         required=true,
     *         @OA\Schema(
     *           type="array",
     *           @OA\Items(type="string"),
     *         ),
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *         name="password",
     *         in="query",
     *         description="User's Password",
     *         required=true,
     *         @OA\Schema(
     *           type="array",
     *           @OA\Items(type="string"),
     *         ),
     *         style="form"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="data",
     *                         type="array",
     *                         description="The response data array",
     *                         @OA\Items(
     *                              @OA\Property(
     *                                  property="access_token",
     *                                  type="string",
     *                                  description="the Access Token usable for authenticating"
     *                              )
     *                         )
     *                     ),
     *                     example={
     *                         "data": {
     *                              "access_token": "the access token string"
     *                          },
     *                         "meta" : { "status":{} }
     *                     }
     *                 )
     *             )
     *         },
     *         description="This is a successful response mode. If the client receives an access_token, it means that the login was done successfuly",
     *         @OA\Schema(
     *             type="array",
     *             format="api response",
     *         ),
     *     ),
     *     @OA\Response(
     *         response=401,
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="meta",
     *                         type="array",
     *                         description="Unbauthorized request response",
     *                         @OA\Items(
     *                              @OA\Property(
     *                                  property="status_code",
     *                                  type="integer",
     *                                  description="The http response status code"
     *                              ),
     *                              @OA\Property(
     *                                  property="message",
     *                                  type="string",
     *                                  description="Error message to be displayed to user"
     *                              ),
     *                         )
     *                     ),
     *                     example={
     *                         "data" : {},
     *                         "meta": {
     *                              "status":{
     *                                  "status_code": 401,
     *                                  "message": "We cant find an account with this credentials. Please make sure you entered the right information."
     *                              }
     *                          }
     *                     }
     *                 )
     *             )
     *         },
     *         description="if received an access_token, it means that the login was done successfuly",
     *         @OA\Schema(
     *             type="array",
     *             format="api response",
     *         ),
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Description of the occured error(In the same format of 401 status)",
     *         @OA\Schema(
     *             type="array",
     *             format="api response",
     *         ),
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Description of the occured error(In the same format of 401 status)",
     *         @OA\Schema(
     *             type="array",
     *             format="api response",
     *         ),
     *     ),
     *     security={},
     *     deprecated=false
     * )
     */
    public function login(Request $request)
    {

        $credentials = $request->only('email', 'password');
        $validation = $this->validateLogin($credentials);
        if ($validation->fails()) {
            return $this->respondValidationError($validation);
        }

        $tokenResponse = ['data' => ['access_token' => '']];

        try {

            $userToCheck = $this->checkUserPass($credentials);

            // attempt to verify the credentials and create an access token for the user
            if (!$userToCheck || !$token = JWTAuth::attempt($credentials)) {
                throw new Exception(trans('message.login.failed'), 401);
            }
        } catch (Exception $e) {
            // something went wrong while attempting to encode the token
            return $this->respondWithError($e->getMessage(), $e->getCode());
        }

        // all good so return the token
        $this->message = trans('message.login.success');
        $tokenResponse['data']['access_token'] = $token;
        return $this->respond($tokenResponse);
    }


    /**
     * Validate the user login request.
     *
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    protected function validateLogin($credentials)
    {
        $rules = [
            'email' => 'required|string|email',
            'password' => 'required|string',
        ];

        return Validator::make($credentials, $rules);
    }

    private function checkUserPass($credentials)
    {
        $user = User::where('email', $credentials['email'])->first();
        if ($user && password_verify($credentials['password'], $user->password)) {
            return $user;
        }
        return false;
    }


}
