<?php

namespace App\Http\Controllers\API\V1\Categories;

use App\Http\Controllers\API\ApiController;
use App\Libs\Transformers\V1\CategoryTransformer;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends ApiController
{

    /**
     * @OA\Get(
     *     path="/api/v1/category",
     *     summary="get categories",
     *     tags={"categories.index"},
     *     description="This method returns the list of all categories. it don't get any paramater",
     *     operationId="getFilteredCategory",
     *     @OA\Response(
     *         response=200,
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="data",
     *                         type="array",
     *                         description="The response data array",
     *                         @OA\Items(
     *                              @OA\Property(
     *                                  property="id",
     *                                  type="string",
     *                                  description="the encoded id of the category"
     *                              ),
     *                              @OA\Property(
     *                                  property="title",
     *                                  type="string",
     *                                  description="the string title of the category"
     *                              )
     *                         )
     *                     ),
     *                     example={
     *                         "data": {
     *                              "id": "the encoded id of the category",
     *                              "title": "the string title of the category"
     *                          },
     *                         "meta" : { "status":{} }
     *                     }
     *                 )
     *             )
     *         },
     *         description="This is a successful response mode.",
     *         @OA\Schema(
     *             type="array",
     *             format="api response"
     *         ),
     *     ),
     *     @OA\Response(
     *         response=401,
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="meta",
     *                         type="array",
     *                         description="Unbauthorized request response",
     *                         @OA\Items(
     *                              @OA\Property(
     *                                  property="status_code",
     *                                  type="integer",
     *                                  description="The http response status code"
     *                              ),
     *                              @OA\Property(
     *                                  property="message",
     *                                  type="string",
     *                                  description="Error message to be displayed to user"
     *                              ),
     *                         )
     *                     ),
     *                     example={
     *                         "data" : {},
     *                         "meta": {
     *                              "status":{
     *                                  "status_code": 401,
     *                                  "message": "We can't authorized the users's token."
     *                              }
     *                          }
     *                     }
     *                 )
     *             )
     *         },
     *         description="return an array of categories",
     *         @OA\Schema(
     *             type="array",
     *             format="api response",
     *         ),
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Description of the occured error(In the same format of 401 status)",
     *         @OA\Schema(
     *             type="array",
     *             format="api response",
     *         ),
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Description of the occured error(In the same format of 401 status)",
     *         @OA\Schema(
     *             type="array",
     *             format="api response",
     *         ),
     *     ),
     *     security={},
     *     deprecated=false
     * )
     */

    public function index(Request $request){

        $categories = Category::filter(['id','category_title']);

        return $this->respondWithTransform($categories, new CategoryTransformer());

    }
}
