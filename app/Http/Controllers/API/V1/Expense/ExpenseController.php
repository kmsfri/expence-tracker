<?php

namespace App\Http\Controllers\API\V1\Expense;

use App\Http\Controllers\API\ApiController;
use App\Libs\Transformers\V1\ExpenseTransformer;
use App\Models\Expense;
use Illuminate\Http\Request;
use Validator;

class ExpenseController extends ApiController
{

    /**
     * @OA\Post(
     *     path="/api/v1/expense/store",
     *     summary="store new expense",
     *     tags={"expense.store"},
     *     description="This method get the expense parameters and store in db",
     *     operationId="storeExpense",
     *     @OA\Response(
     *         response=200,
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="data",
     *                         type="array",
     *                         description="The response data array",
     *                         @OA\Items(
     *                              @OA\Property(
     *                                  property="id",
     *                                  type="string",
     *                                  description="the encoded id of the new stored expense"
     *                              ),
     *                              @OA\Property(
     *                                  property="category_id",
     *                                  type="string",
     *                                  description="the encoded category_id of the new stored expense"
     *                              ),
     *                              @OA\Property(
     *                                  property="title",
     *                                  type="string",
     *                                  description="the title of the new stored expense"
     *                              ),
     *                              @OA\Property(
     *                                  property="amount",
     *                                  type="integer",
     *                                  description="the amount of the new stored expense"
     *                              ),
     *                              @OA\Property(
     *                                  property="expense_at",
     *                                  type="string",
     *                                  description="the expense at of the new stored expense"
     *                              ),
     *                              @OA\Property(
     *                                  property="expense_description",
     *                                  type="string",
     *                                  description="the expense description of the new stored expense"
     *                              )
     *                         )
     *                     ),
     *                     example={
     *                         "data": {
     *                              "id": "the encoded id of the expense",
     *                              "category_id": "the encoded category id of the expense",
     *                              "title": "the string title of the expense",
     *                              "amount": "the integer amount of the expense",
     *                              "expense_at": "the datetime expense_at of the expense",
     *                              "expense_description": "the string expense_description of the expense"
     *                          },
     *                         "meta" : { "status":{} }
     *                     }
     *                 )
     *             )
     *         },
     *         description="This is a successful response mode.",
     *         @OA\Schema(
     *             type="array",
     *             format="api response"
     *         ),
     *     ),
     *     @OA\Response(
     *         response=401,
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="meta",
     *                         type="array",
     *                         description="Unbauthorized request response",
     *                         @OA\Items(
     *                              @OA\Property(
     *                                  property="status_code",
     *                                  type="integer",
     *                                  description="The http response status code"
     *                              ),
     *                              @OA\Property(
     *                                  property="message",
     *                                  type="string",
     *                                  description="Error message to be displayed to user"
     *                              ),
     *                         )
     *                     ),
     *                     example={
     *                         "data" : {},
     *                         "meta": {
     *                              "status":{
     *                                  "status_code": 401,
     *                                  "message": "We can't authorized the users's token."
     *                              }
     *                          }
     *                     }
     *                 )
     *             )
     *         },
     *         description="return an authorize response error",
     *         @OA\Schema(
     *             type="array",
     *             format="api response",
     *         ),
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Description of the occured error(In the same format of 401 status)",
     *         @OA\Schema(
     *             type="array",
     *             format="api response",
     *         ),
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Description of the occured error(In the same format of 401 status)",
     *         @OA\Schema(
     *             type="array",
     *             format="api response",
     *         ),
     *     ),
     *     security={},
     *     deprecated=false
     * )
     */
    public function store(Request $request)
    {

        try {

            $validate = [
                'category_id' => 'nullable',
                'expense_title' => 'required|max:255',
                'expense_amount' => 'required|integer',
                'expense_at' => 'nullable|max:15',
                'expense_description' => 'nullable|max:255',
            ];

            $validatedData = Validator::make($request->all(), $validate);

            if ($validatedData->fails()) {
                return $this->respondValidationError($validatedData);
            }

            $expense = Expense::createNewExpense($request->all());

        } catch (Exception $e) {

            return $this->respondWithError($e->getMessage(), 400);

        }

        $this->message = trans('message.expense.store');
        return $this->respondWithTransform($expense, new ExpenseTransformer());

    }
}
