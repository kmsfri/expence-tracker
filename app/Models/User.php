<?php

namespace App\Models;

use App\Casts\Serialize;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Mtvs\EloquentHashids\HasHashid;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Models\Expense;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable, HasHashid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'created_at' => Serialize::class,
        'updated_at' => Serialize::class
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->idToHashid($this->getKey());
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function expenses()
    {
        return $this->hasMany(Expense::class, 'user_id');
    }

    public function getSumOfUserExpenses(){
        return $this->expenses()->sum('expense_amount');
    }
}
