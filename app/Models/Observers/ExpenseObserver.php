<?php

namespace App\Models\Observers;

use App\Libs\Helpers\ExpenseHelper;
use App\Models\Expense;
use App\Models\User;
use Mail;

class ExpenseObserver
{
    public function created(Expense $expense)
    {
        $user = User::find($expense->user_id);
        $sumOfUserExpenses = ExpenseHelper::getUserSumExpenses($user);

        //TODO: dispatch and send notify mail. data: $sumOfUserExpenses

    }

}