<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Vinkla\Hashids\Facades\Hashids;


class Category extends Model
{
    use HasFactory;

    const ITEM_PER_PAGE = 10;
    const DEFAULT_ORDER_BY = 'category_order';
    const DEFAULT_ORDER_DIR = 'ASC';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];


    public function expenses()
    {
        return $this->hasMany(Expense::class, 'category_id');
    }

    public static function filter($requiredFields = '*', $filterParams = [], $orderBy = self::DEFAULT_ORDER_BY, $orderDir = self::DEFAULT_ORDER_DIR, $paginate = false, $itemPerPage = self::ITEM_PER_PAGE)
    {
        $result = self::select($requiredFields)
            ->where($filterParams)
            ->orderBy($orderBy, $orderDir);

        $result = $paginate ?
            $result->paginate($itemPerPage) :
            $result->get();

        return $result;
    }

    public function getIdAttribute($value)
    {
        return Hashids::encode($value);
    }
}
