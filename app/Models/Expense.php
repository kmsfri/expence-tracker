<?php

namespace App\Models;

use App\Casts\Serialize;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Mtvs\EloquentHashids\HasHashid;
use Vinkla\Hashids\Facades\Hashids;

class Expense extends Model
{
    use HasFactory, HasHashid;

    protected $primaryKey = 'id';
    protected $guarded = ['user_id'];

    protected $casts = [
        'created_at' => Serialize::class,
        'updated_at' => Serialize::class,
    ];

    protected $fillable = [
        'user_id', 'category_id', 'expense_title', 'expense_amount', 'expense_at', 'expense_description'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function setCategoryIdAttribute($value)
    {
        $this->attributes['category_id'] = $this->hashidToId($value);
    }

    public function getIdAttribute($value)
    {
        return Hashids::encode($value);
    }

    public function getCategoryIdAttribute($value)
    {
        return Hashids::encode($value);
    }

    public static function createNewExpense($requestData)
    {

        $allData = array_merge($requestData, ['user_id' => request()->user()->id]);

        $expense = Expense::create($allData);

        return $expense;

    }
}
