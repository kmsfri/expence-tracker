<?php

/*
 * This file is part of jwt-auth.
 *
 * (c) Sean Tymon <tymon148@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Providers;

use Illuminate\Contracts\Auth\Guard as GuardContract;
use Tymon\JWTAuth\Contracts\Providers\Auth;
use Tymon\JWTAuth\Providers\Auth\Illuminate as JwtIlluminate;
use Vinkla\Hashids\Facades\Hashids;

class JwtAuthServiceProvider extends JwtIlluminate
{

    /**
     * Authenticate a user via the id.
     *
     * @param mixed $id
     *
     * @return bool
     */
    public function byId($id)
    {
        $id = Hashids::decode($id);
        return $this->auth->onceUsingId($id);
    }
}
