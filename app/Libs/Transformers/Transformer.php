<?php

namespace App\Libs\Transformers;

use League\Fractal;


class Transformer extends Fractal\TransformerAbstract
{

    protected static $fractal;

    public function __construct()
    {
        if (!isset(static::$fractal)) {
            static::$fractal = new Fractal\Manager();
        }
    }

    public function getFractal()
    {
        return static::$fractal;
    }


}
