<?php

namespace App\Libs\Transformers\V1;

use App\Libs\Transformers\Transformer;
use App\Models\Category;

class CategoryTransformer extends Transformer
{

    public function transform(Category $category)
    {
        $response = [

            'id' => $category->id,
            'title' => $category->category_title,

        ];

        return $response;
    }

}
