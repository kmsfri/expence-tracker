<?php

namespace App\Libs\Transformers\V1;

use App\Libs\Transformers\Transformer;
use App\Models\Expense;

class ExpenseTransformer extends Transformer
{

    public function transform(Expense $expense)
    {
        $response = [

            'id' => $expense->id,
            'category_id' => $expense->category_id,
            'title' => $expense->expense_title,
            'amount' => $expense->expense_amount,
            'expense_at' => $expense->expense_at,
            'expense_description' => $expense->expense_description,

        ];

        return $response;
    }

}
