<?php

namespace App\Libs\Helpers;

use App\Models\User;

class ExpenseHelper
{
    public static function getUserSumExpenses(User $user)
    {
        $sumOfExpenses = $user->getSumOfUserExpenses();

        return $sumOfExpenses;
    }
}
