<?php

namespace App\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Carbon\Carbon;

class Serialize implements CastsAttributes
{

    /***
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param \Illuminate\Contracts\Database\Eloquent\string|string $key
     * @param mixed $value
     * @param array $attributes
     * @return mixed|string
     * @throws \Exception
     *
     */

    public function get($model, $key, $value, $attributes)
    {
        return (new Carbon($value))->format('Y-m-d H:i:s');
    }

    /**
     * Prepare the given value for storage.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return mixed
     */
    public function set($model, $key, $value, $attributes)
    {
        return $value;
    }
}
