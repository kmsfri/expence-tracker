<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::select('*')->delete();

        Category::insert([
            [
                'category_title' => 'Personal',
                'category_order' => '1'
            ],
            [
                'category_title' => 'Work and Business',
                'category_order' => '2'
            ],
            [
                'category_title' => 'Tax and bills',
                'category_order' => '3'
            ],
            [
                'category_title' => 'Daily expenses',
                'category_order' => '4'
            ],
            [
                'category_title' => 'Unexpected expenses',
                'category_order' => '5'
            ],
            [
                'category_title' => 'Fun and Entertainment expenses',
                'category_order' => '6'
            ],
            [
                'category_title' => 'Other expenses',
                'category_order' => '7'
            ]
        ]);
    }
}
