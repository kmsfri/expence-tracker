FROM alpine:3.13

LABEL Maintainer="Kamyar Safari <kmsfri@gmail.com>"

RUN apk --no-cache add php7 php7-fpm php7-opcache php7-mysqli php7-pdo php7-pdo_mysql php7-json php7-openssl php7-curl \
    php7-zlib php7-xml php7-phar php7-intl php7-dom php7-xmlreader php7-ctype php7-session php7-iconv \
    php7-mbstring php7-xmlwriter php7-exif php7-redis php7-tokenizer php7-fileinfo php7-gd php7-bcmath nginx supervisor curl && \
    rm /etc/nginx/conf.d/default.conf

# Configure nginx
COPY ./docker/nginx.conf /etc/nginx/nginx.conf
COPY ./docker/fpm-pool.conf /etc/php7/php-fpm.d/www.conf
COPY ./docker/php.ini /etc/php7/conf.d/custom.ini
COPY ./docker/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

WORKDIR /var/www/html

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --version="2.0.0" --filename=composer  | php -- --install-dir=/usr/bin --filename=composer

COPY . .

COPY sample.env .env

RUN chmod 777 -R ./bootstrap/cache

RUN composer clear-cache && \
    composer config --global repo.packagist composer https://packagist.org && \
    COMPOSER_MEMORY_LIMIT=-1 composer install --no-ansi --no-dev --no-interaction --no-plugins --no-progress --no-scripts --optimize-autoloader --prefer-dist && \
    composer dump-autoload

ENV USERID=1000

RUN addgroup -S -g $USERID expencetracker && adduser -S -u $USERID -G expencetracker expencetracker && \
     chown -R expencetracker:expencetracker /var/www/html && \
     chmod -R ug+rwx storage bootstrap/cache && \
     sed -i "s/user\ \=.*/user\ \= expencetracker/g" /etc/php7/php-fpm.d/www.conf && \
     sed -i "s/group\ \=.*/group\ \= expencetracker/g" /etc/php7/php-fpm.d/www.conf && \
     chown -R expencetracker:expencetracker /var/lib/nginx && \
     chown -R expencetracker:expencetracker /var/log/nginx && \
     chown -R expencetracker:expencetracker /run

USER expencetracker

EXPOSE 8080

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]

HEALTHCHECK --timeout=10s CMD curl --silent --fail http://127.0.0.1:8080/fpm-ping

