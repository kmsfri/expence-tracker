<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Version 1 Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Users\AuthController@login');

Route::group(['middleware' => ['jwt.auth', 'throttle:6,1']], function() {

    Route::group(['prefix' => 'category'], function () {
        Route::get('/', 'Categories\CategoryController@index');
    });

    Route::group(['prefix' => 'expense'], function () {
        Route::post('/store', 'Expense\ExpenseController@store');
    });

});
