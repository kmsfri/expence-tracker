<h1 align="center"> Expense Tracker </h1> <br>

<p align="center">
  A test task for hiring at snapp! An expense tracker. An app to add and categorize user expenses. provide custom alerts for things, like, “spending too much money on coffee… as always.”
</p>

## Introduction

This project is a series of RESTful APIs Which stores the expenses of each user in the database And reports the total expenses to the user each time 

**Available for web clients.**

## Features

A list of things you can do with ExpenseTracker APIs:

* Login
* Get Categories list
* Store expenses of each category one by one
* Get notify each time one of the expenses is saved 

## Build Process
You must have docker and docker-composer on your system to run this project

- `cp sample.env .env` to copy all of sample.env config files to the .env config file
- `docker-compose up -d --build` to build server requirements on docker and install dependencies
- `docker exec -it et.app sh` go to the app container shell
- `composer install` to get package and dependencies. You should run this in the app container shell
- `php artisan migrate` to run db migrations. You should run this in the app container shell
- `php artisan db:seed` run database seeders for generate initial categories and user. You should run this in the app container shell

## API documentations
You can access the swagger documentations via this endpoint:
`/api/documentation`

The `postman collection` file accessible via this link:
<a href="https://www.dropbox.com/s/p6jhyezz4kks3b5/expencetracker.postman_collection.json?dl=1">https://www.dropbox.com/s/p6jhyezz4kks3b5/expencetracker.postman_collection.json?dl=1</a>

The `postman environment` file accessible via this link:
<a href="https://www.dropbox.com/s/w7dzr2bu7cpekwc/ExpenseTracker.postman_environment.json?dl=1">https://www.dropbox.com/s/w7dzr2bu7cpekwc/ExpenseTracker.postman_environment.json?dl=1</a>

**Keys**:
The Initial email And password that inserted by seeder is`test@test.com` and `123456` that used for login by `api/v1/login`